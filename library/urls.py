from django.urls import path

from . import views

urlpatterns = [    
    path('index', views.index, name='index'),
    path('end', views.end, name='end'),
]
