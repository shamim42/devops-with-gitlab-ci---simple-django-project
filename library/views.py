from django.http import HttpResponse

def index(request):
    return HttpResponse("<h1>Bismillahir Rahmanir Rahim</h1>")

def end(request):
    return HttpResponse("<h2>Alhamdulillahi Robbil Alameen</h2>")