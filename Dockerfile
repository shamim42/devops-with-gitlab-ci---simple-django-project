FROM python:3.6.2
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code

RUN echo "deb http://deb.debian.org/debian jessie main" > /etc/apt/sources.list &&\
    echo "deb http://security.debian.org jessie/updates main" >> /etc/apt/sources.list
RUN apt-get update && apt-get install -y gettext

ADD requirements.txt /code/
RUN pip install -r requirements.txt

ADD . /code/